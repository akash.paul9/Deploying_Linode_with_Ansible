## Install and Configure Ansible

Install the latest version of Ansible on your computer:

> pip3 install --user ansible

Then install the official Python library for the Linode API:

> pip3 install linode_api4

Configure Ansible, we'll use a local config file ansible.cfg within the working directory:

```
[defaults]
host_key_checking = False
VAULT_PASSWORD_FILE = ./vault-pass
[inventory]
enable_plugins = linode
```
## Create Linode Playbook

Create a new playbook linode_create.yml:

```
- name: Create Linode
  hosts: localhost
  vars_files:
      - ./vars
  tasks:
  - name: Create a new Linode.
    linode_v4:
      label: "{{ label }}{{ 100 |random }}"
      access_token: "{{ token }}"
      type: g6-nanode-1
      region: eu-west
      image: linode/ubuntu20.04
      root_pass: "{{ password }}"
      authorized_keys: "{{ ssh_keys }}"
      tags: example_group
      state: present
    register: my_linode
  - name: Print info about my Linode instance
    debug:
      msg: "ID is {{ my_linode.instance.id }} IP is {{ my_linode.instance.ipv4 }}"
```
This uses Jinja template variables so we need to create the initial variable file ./vars

> sudo nano ./vars

```
ssh_keys: >
        ['~/.ssh/id_rsa.pub']
label: simple-linode
```
Here ssh keys will be copied to the linode. Label is the Linode display label

Sensitive variables will be encrypted using ansible-vault. Create a vault password in ./vault-pass

> sudo nano ./vault-pass

Put a strong password in the file.

`Passw0rd+`

Encrypt it using:

> ansible-vault encrypt_string 'Passw0rd+' --name 'password'

You may ask to install ansible by apt.

> sudo apt install ansible

The resulting output is:

```
password: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          63373134363837623334623932323766623062326364613635633338356131336361333237383362
          3339376362353432323131626261346339323766653232380a613663643336626661373639653638
          31383961353736656138333436653331313135313534316636636266616363653836303436326537
          3761663164356462630a356239356338343763663930383438343039313363616237356663316436
          3134
Encryption successful
```

Add this to the bottom of the ./vars file:

```
password: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          63373134363837623334623932323766623062326364613635633338356131336361333237383362
          3339376362353432323131626261346339323766653232380a613663643336626661373639653638
          31383961353736656138333436653331313135313534316636636266616363653836303436326537
          3761663164356462630a356239356338343763663930383438343039313363616237356663316436
          3134
```

Go to your Linode dashboard, click your user name top right and select My Profile in the drop-down menu. Then select API Tokens and add a personal access token with Linodes Read/Write permissions. Copy this toekn and encrypt it using:

> ansible-vault encrypt_string '<Your Token> ' --name 'token'

Add the output to the bottom of the ./vars file:

```
token: !vault |
          $ANSIBLE_VAULT;1.1;AES256
          63626164373261323637343737313634653730656663386431333133303638363635313462323864
          6365613334643836373032623233306336393065353138620a653363653266343532386464613762
          66353130303639313734353465326131386536393437356564383564353037383437626364636638
          3566393537383365630a333737376431626261313062626335383630656234316237316464376462
          3763
```

## Run the Playbook

You are now ready to run the playbook:

> ansible-playbook linode_create.yml

Which should give the following output:

```
PLAY [Create Linode] ********************************************************************************************************************************

TASK [Gathering Facts] ******************************************************************************************************************************
ok: [localhost]

TASK [Create a new Linode.] *************************************************************************************************************************
changed: [localhost]

TASK [Print info about my Linode instance] **********************************************************************************************************
ok: [localhost] => {
    "msg": "ID is XXXXXXXX IP is ['AAA.BB.CCC.DDD']"
}

PLAY RECAP ******************************************************************************************************************************************
localhost                  : ok=3    changed=1    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```
A glance at your Linode dashboard should show the new Linode instance. That's it, you're all set.

You can change region, package name in playbook. By using these command you can get region, package name list.

> curl https://api.linode.com/v4/regions/

> curl https://api.linode.com/v4/linode/types

REF: https://amrs.uk/posts/2020-08-24-ansible-linode/
